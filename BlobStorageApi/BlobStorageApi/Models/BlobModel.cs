﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlobStorageApi.Models
{
    public class BlobModel
    {
        public BlobModel(string BlobId)
        {
            this.BlobId = BlobId;
        }
        public string BlobId { get; set; }
    }
}