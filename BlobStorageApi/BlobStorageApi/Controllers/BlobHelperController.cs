﻿using BlobStorageApi.Helper;
using BlobStorageApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Http;

namespace BlobStorageApi.Controllers
{
    [RoutePrefix("blob")]
    public class BlobHelperController : ApiController
    {
        private BlobHandler handler;
        public BlobHelperController()
        {
            handler = new BlobHandler(ConfigurationManager.AppSettings["ContainerDirectory"]);
        }

        [HttpPost]
        [Route("store")]
        public IHttpActionResult StoreDataToBlob()
        {           
            try
            {
               HttpFileCollection fileCollection = HttpContext.Current.Request.Files;

               foreach (String key in fileCollection.AllKeys)
               {
                    string k = key.ToString();

                    string blob = handler.StoreFileToBlob(fileCollection.Get(key)); 
               }
               return Ok("Data Stored:");
            }
            catch (Exception)
            {
                return Content(System.Net.HttpStatusCode.InternalServerError, "Server Error");
            }
        }

        [HttpDelete]
        [Route("removefile")]
        public IHttpActionResult DeleteDataFromBlob([FromBody]BlobModel blobModel)
        {
            try
            {
                var result = handler.DeleteFileFromBlob(blobModel.BlobId);
                return Ok("Data Deleted:" + result);
            }
            catch (Exception)
            {
                return Content(System.Net.HttpStatusCode.InternalServerError, "Server Error");
            }
        }

        [HttpGet]
        [Route("filedownload")]
        public IHttpActionResult GetBlobWithSASToken([FromBody]BlobModel blobModel)
        {
            try
            {
                var uriToken = handler.GetBlobSASTokenForBlob(blobModel.BlobId);
                return Ok("Uri Token:" + uriToken);
            }
            catch(Exception)
            {
                return Content(System.Net.HttpStatusCode.InternalServerError, "Server Error");
            }
        }
    }
}