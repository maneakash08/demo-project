﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlobStorageApi.Helper
{
    public class BlobHandler
    {
        private CloudStorageAccount account;
        private CloudBlobClient client;
        private CloudBlobContainer container;

        private const string BLOBCS = @"DefaultEndpointsProtocol=https;AccountName=azurestorageaccount08;AccountKey=JT1T8kLolOR6vB7YXyCF34DFglL6eSWWOhzTAD5xybvD6JR6onff6JvbDSRfUo2ozLSgI8kr2J3ccnATjz//oQ==;EndpointSuffix=core.windows.net";

        public BlobHandler(string ContainerName, string StorageConnectionString = BLOBCS)
        {
            account = CloudStorageAccount.Parse(StorageConnectionString);
            client = account.CreateCloudBlobClient();
            container = client.GetContainerReference(ContainerName);
        }

        public string GetBlobSASTokenForBlob(string blobid)
        {
            CloudBlockBlob blob = this.container.GetBlockBlobReference(blobid);
            SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy()
            {
                SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddHours(2),
                SharedAccessStartTime = DateTime.Now.ToLocalTime(),
                Permissions = SharedAccessBlobPermissions.Read & SharedAccessBlobPermissions.Write
            };
            string token = blob.GetSharedAccessSignature(policy);
            // Open link in Browser
            return blob.Uri + token;
        }

        public string StoreFileToBlob(HttpPostedFile file)
        {
            string NewFileName = file.FileName.Replace(" ","_");
            CloudBlockBlob blob = this.container.GetBlockBlobReference(NewFileName);
            blob.Properties.ContentType = file.ContentType;
            blob.UploadFromStream(file.InputStream);

            return blob.Name.ToString();
        }

        public bool DeleteFileFromBlob(string blobid)
        {
            CloudBlockBlob blob = this.container.GetBlockBlobReference(blobid);
            return blob.DeleteIfExists();
        }
    }
}
