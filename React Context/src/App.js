import React, { Component } from 'react';
import './App.css';
import { EventEmitter } from 'events';
import './index.css';
import BaseEventEmitter from './Api/emitter';

const TestContext = React.createContext();
// export const emitter = new BaseEventEmitter();
const emitter = new BaseEventEmitter();

class Cartitem {
  constructor(Id, Name, Qty, Price) {
    this.Id = Id
    this.Name = Name
    this.Qty = Qty
    this.Price = Price
  }
}

class CartList extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    cart: []
  }

  componentWillReceiveProps(nextProps) {
    if (this.props != nextProps) {
      this.setState(nextProps);
      console.log(nextProps);
    }
  }

  static contextType = TestContext;

  searchTask = (event) => {
    let currentList = [];
    let newList = [];

    if (event.target.value !== "") {
      currentList = this.context.cart;

      newList = currentList.filter(item => {
        const cartName = item.cart.toLowerCase();
        const filter = event.target.value.toLowerCase();

        return cartName.includes(filter);
      });
    } else {
      newList = this.context.cart;
    }

    this.setState({
      cart: newList
    });
  }

  render() {
    return (<React.Fragment>
      <div></div>{this.context.title}
      <div>
        <ul>
          {this.context.getItems().map((item, index) => (
            <li key={index}>ID:{index + 1}-Title:{item.Name}</li>
          ))}
        </ul>
      </div>

    </React.Fragment>);
  }
}

class TOperationContext extends Component {

  state = {
    title: "Dark",
    cart: []
  }

  static contextType = TestContext;

  render() {
    return (<TestContext.Provider value={
      {
        title: this.state.title,
        getItems: () => {
          return this.state.cart
        },
        changeTheme: () => {
          this.setState({ title: this.state.title === "Dark" ? "Light" : "Dark" });
        },
        addItem: (cartItem) => {
          if (cartItem != undefined) {
              this.state.cart.push(cartItem);
              this.setState({ cart: this.state.cart });
          } else {
            alert("Enter Cart item Name")
          }
        }
      }
    }>
      <TTestContext></TTestContext>
      <div className={this.state.title}>Theme:{this.state.title}</div>
    </TestContext.Provider>);
  }
}

class TTestContext extends Component {

  static contextType = TestContext

  state = {
    checkThemePreference: false
  }

  componentDidMount() {
    emitter.on("change-theme", () => {
      this.setState({ checkThemePreference: !this.state.checkThemePreference });
    })
  }

  consumerContextTest = () => {
    return (<TestContext.Consumer>
      {value => console.log("val:", value.getItems())}
    </TestContext.Consumer>);
  }

  render() {
    return (<div>
      {this.consumerContextTest()}
      {this.context.title}
      <br></br>
      <AddCart></AddCart>
      <CartList cartItems={this.context.getItems()}></CartList>
      {
        this.state.checkThemePreference == true ?
          (<input type="button" value="Theme Change" onClick={this.context.changeTheme} />) :
          (<div>Change Theme Preference</div>)
      }
    </div>);
  }
}

class AddCart extends Component {
  constructor(props) {
    super(props);
    this.form = [{
      type: "text",
      placeholder: "Enter Name",
      required: false,
      name: "p-name",
      value: ""
    }]

    this.val = [];
  }

  state = {
    value: [],
  }

  changeThemePref = () => {
    emitter.emit("change-theme");
  }

  static contextType = TestContext

  handleChange(index, event) {
    this.val[index] = { key: event.target.name, value: event.target.value };

    this.setState({
      value: this.val
    });
  }

  createCartObject = () => {
    var obj = this.state.value[0];
    if (obj != undefined) {
      var item = new Cartitem(1, this.state.value[0].value, 1, 450000);
      return item;
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.form.map((el, index) => (
          <input
            key={index}
            type={el.type}
            name={el.name}
            required={el.required}
            placeholder={el.placeholder}
            onChange={this.handleChange.bind(this, index)}>
          </input>
        ))}

        <input type="button" value="Theme Preference" onClick={this.changeThemePref} />
        <div>Add Item</div>
        <input type="button"
          value="Add item to Cart"
          onClick={() => this.context.addItem(this.createCartObject())} />
      </React.Fragment>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <TOperationContext />
      </div>);
  }

  componentWillUnmount() {
    emitter.off("test", () => {
      console.log("Test Emitter is Distroyed");
    });
  }
}

export default App;
