import { EventEmitter } from "events";

export default class BaseEventEmitter {
    constructor() {
        this.eventEmitter = new EventEmitter();
    }

    on(eventName,listeners) {
        this.eventEmitter.on(eventName,listeners);
    }

    off(eventName,listeners) {
        this.eventEmitter.off(eventName,listeners);
    }

    emit(eventName,payload,error = false) {
        this.eventEmitter.emit(eventName,payload,error);
    }
}