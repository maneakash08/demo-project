﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Web;
using System.IO;
using EmCES.Candidates.Command.Business.Entities;
using System.Text.RegularExpressions;
using EmCES.Candidates.Query.Core;

namespace EmCES.Candidates.Command.Core
{
    public class SendMailHelper
    {
        //Reusable Send Mail Method
        public static void sendMessageAny(string subject,string messageData,bool attachment,string toEmailId)
        {
            int i = 0;
            string mailId = System.Configuration.ConfigurationManager.AppSettings["mailId"];
            string sendGridApiKey = System.Configuration.ConfigurationManager.AppSettings["sendGridApiKey"];
            string AppName = "Candidate Engagement System";
            string _toMailId = toEmailId;

            try
            {
                var client = new SendGridClient(sendGridApiKey);

                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(mailId, AppName),
                    Subject = subject
                };

                messageData.Replace("\n", Environment.NewLine);

                if(attachment)
                {
                    HttpFileCollection files = HttpContext.Current.Request.Files;

                    foreach (String key in files.AllKeys)
                    {
                        int did = Int32.Parse(key);
                        HttpPostedFile hpf = files[i];
                        BinaryReader b = new BinaryReader(hpf.InputStream);
                        string contents = String.Empty;
                        using (var memoryStream = new MemoryStream())
                        {
                            hpf.InputStream.CopyToAsync(memoryStream).Wait();
                            byte[] stream = memoryStream.ToArray();
                            string content = Convert.ToBase64String(stream);
                            contents = content;
                        }
                        msg.PlainTextContent = messageData;
                        msg.AddAttachment(hpf.FileName, contents);
                        i++;
                    }
                }

                msg.PlainTextContent = messageData;

                msg.AddTo(new EmailAddress(toEmailId, "User Email"));
                var response = client.SendEmailAsync(msg);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static void sendMessage(string subject, string message)
        {
            var i = 0;
            String uploadedDocs = "Uploaded Document List :" + System.Environment.NewLine + System.Environment.NewLine;
            String MailId = System.Configuration.ConfigurationManager.AppSettings["mailId"];
            String SendGridAPIKey = System.Configuration.ConfigurationManager.AppSettings["sendGridApiKey"];

            try
            {
                System.Environment.SetEnvironmentVariable("apikey", SendGridAPIKey);
                var apiKey = System.Environment.GetEnvironmentVariable("apikey");
                var client = new SendGridClient(apiKey);

                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(MailId, "Candidate Engagement System"),
                    Subject = subject
                };

                //String.Join(Environment.NewLine, message.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries));

                message.Replace("\n", Environment.NewLine);

                HttpFileCollection files = HttpContext.Current.Request.Files;

                foreach (String key in files.AllKeys)
                {
                    int did = Int32.Parse(key);
                    HttpPostedFile hpf = files[i];
                    BinaryReader b = new BinaryReader(hpf.InputStream);
                    string contents = String.Empty;
                    using (var memoryStream = new MemoryStream())
                    {
                        hpf.InputStream.CopyToAsync(memoryStream).Wait();
                        byte[] stream = memoryStream.ToArray();
                        string content = Convert.ToBase64String(stream);
                        contents = content;
                    }

                    msg.PlainTextContent = message;
                    msg.AddAttachment(hpf.FileName, contents);
                    i++;
                }

                msg.AddTo(new EmailAddress(MailId, "User Email"));
                var response = client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //For Blob Storage
        //[HttpPost]
        //[Route("storedoc")]
        public void Store()
        {
            try
            {
                HttpFileCollection postfilecollection = HttpContext.Current.Request.Files;

                foreach (String key in postfilecollection.AllKeys)
                {
                    BlobHandler bh = new BlobHandler("rookiestestontainer");
                    string blob = bh.StoreFileToBlob(postfilecollection.Get(key));
                    CandidateDocument doc = new CandidateDocument();
                    doc.CandidateId = Int32.Parse(HttpContext.Current.Request.Params.Get("CandidateId"));
                    doc.Notes = HttpContext.Current.Request.Params.Get("Notes");
                    doc.DocumentId = Int32.Parse(key);
                    doc.BLOBId = blob;
                    _candidateService.Store(doc);
                }
                //return Ok("Data Stored");
            }
            catch (DbUpdateException ex)
            {
            //    return Content(HttpStatusCode.BadRequest, "invalid input" + ex.Data.ToString());
            }

        }

    }
}
