﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using EmCES.Candidates.Command.Business.Contracts;
using EmCES.Candidates.Command.Business.Entities;
using EmCES.Candidates.Command.Data.Repository;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Web.Http.Cors;
using EmCES.Candidates.Command.Core;
using EmCES.Candidates.Command.WebApi.BlobOps;
using log4net;
using EmCES.Candidates.Command.IOC.Services;

namespace EmCES.Candidates.Command.WebApi.Controllers
{
    //[CESAuthorizeAttribute(Roles = "cand")]
    [RoutePrefix("candidates")]
    public class CandidateController : BaseApiController
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(CandidateController));  
        private readonly ICandidateCommandService _candidateService;
        private StatusCodes codes;

        public CandidateController(ICandidateCommandService candidateService)
        {
            _candidateService = candidateService;
            codes = new StatusCodes();
        }

        [HttpDelete]
        [Route("unassign/{cid}/{bid}")]
        public IHttpActionResult UnassignCandidate(int cid, int bid)
        {
            try
            {
                return Ok(_candidateService.UnassignCandidate(cid, bid));
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, codes.ServerProblem);
            }
        }

        //[HttpPost]
        //[Route("storedoc")]
        //public IHttpActionResult Store(CandidateDocument canDoc)
        //{
        //    try
        //    {
        //        bool Result = _candidateService.Store(canDoc);
        //        return Ok(codes.RequestOk);
        //    }
        //    catch (SqlException ex)
        //    {
        //        return Content(HttpStatusCode.BadRequest, codes.ClientRequestWrong);
        //    }
        //    catch(Exception)
        //    {
        //        return Content(HttpStatusCode.InternalServerError, codes.ServerProblem);
        //    }
        //}

        [HttpPost]
        [Route("storedoc")]
        public IHttpActionResult Store()
        {
            log.Info(string.Format("In Store."));
            try
            {
                int i = 0;
                int cid = base.CandidateId;
                //ApplicationUser appUser = _candidateService.getCandiateData(1);
                //String userDetails = "Uploaded Documents by Candidate : " + appUser.FirstName + " " + appUser.LastName;
                String userDetails = "Uploaded Documents by Candidate : Akash Mane";
                String uploadedDocs = "Uploaded Document List :\n\n";
                String toMailId = "akash.mane@emtecinc.com";

                // + System.Environment.NewLine + System.Environment.NewLine
                HttpFileCollection files = HttpContext.Current.Request.Files;

                foreach (String key in files.AllKeys)
                {
                    int did = Int32.Parse(key);
                    HttpPostedFile hpf = files[i];

                    CandidateDocument doc = new CandidateDocument();
                    doc.CandidateId = cid;
                    doc.Notes = "Notes";
                    doc.DocumentId = Int32.Parse(key);
                    DocumentList singleDocDetails = _candidateService.getAllDoc(doc.DocumentId);
                    uploadedDocs = uploadedDocs + "Document : " + singleDocDetails.Title + "\t - [File Name : " + hpf.FileName + "]   \n\n";
                    doc.BLOBId = hpf.FileName;

                    //_candidateService.Store(doc);
                    i++;
                }
                log.Info(string.Format("Saved documents"));
                //SendMailHelper.sendMessage(userDetails, uploadedDocs);
                SendMailHelper.sendMessageAny(userDetails,uploadedDocs,true,toMailId);
                return Ok("Message Sent");
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                // return Content(HttpStatusCode.BadRequest, new ErrorHandler(e).HandleException());
                //return Content(HttpStatusCode.BadRequest, "invalid input");
                 return Content(HttpStatusCode.InternalServerError, codes.ServerProblem);
            }
        }

        [HttpGet]
        [Route("storedoc/sendmail")]
        public IHttpActionResult sendEmail()
        {
            try
            {
                SendMailHelper.sendMessageAny("Test Mail Subject", "Message", false,"akash.mane@emtecinc.com");
                return Ok("Mail Sent");
            }
            catch
            {
                return Content(HttpStatusCode.InternalServerError,codes.ServerProblem);
            }
        }

        [HttpPut]
        [Route("gamification/{gamificationtype}")]
        public IHttpActionResult SetGamification(int gamificationtype)
        {
            log.Info(string.Format("In SetGamification.  input =>gamificationtype = {0}", gamificationtype));
            try
            {
                log.Info(string.Format("Saved gamificationtype {0}",gamificationtype));
                return Ok(_candidateService.SetGamification(base.CandidateId, gamificationtype));
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                return Content(HttpStatusCode.InternalServerError, codes.ServerProblem);
            }
        }

        [HttpPut]
        [Route("score/{count}")]
        public IHttpActionResult setScore(int count)
        {
            log.Info(string.Format("In setScore. "));
            try
            {
                log.Info(string.Format("Saved score "));
                return Ok(_candidateService.SetScore(base.CandidateId,count));
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                return Content(HttpStatusCode.InternalServerError, codes.ServerProblem);
            }
        }

        //[HttpPost]
        //[Route("storedoc")]
        //public IHttpActionResult Store()
        //{
        //    try
        //    {
        //        HttpFileCollection postfilecollection = HttpContext.Current.Request.Files;

        //        foreach (String key in postfilecollection.AllKeys)
        //        {
        //            BlobHandler bh = new BlobHandler("rookiestestontainer");
        //            string blob = bh.StoreFileToBlob(postfilecollection.Get(key));
        //            CandidateDocument doc = new CandidateDocument();
        //            doc.CandidateId = Int32.Parse(HttpContext.Current.Request.Params.Get("CandidateId"));
        //            doc.Notes = HttpContext.Current.Request.Params.Get("Notes");
        //            doc.DocumentId = Int32.Parse(key);
        //            doc.BLOBId = blob;
        //            _candidateService.Store(doc);
        //        }
        //        return Ok("Data Stored");
        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        return Content(HttpStatusCode.BadRequest, "invalid input" + ex.Data.ToString());
        //    }

        //}


        [HttpPost]
        [Route("meeting/{dt}")]
        public IHttpActionResult setMeeting(DateTime dt)
        {
            log.Info(string.Format("In setMeeting.  input =>date = {0}", dt));
            try
            {
                bool Result = _candidateService.setMeeting(base.CandidateId, dt);
                log.Info(string.Format("Saved date of meeting "));
                return Ok(codes.RequestOk);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                return Content(HttpStatusCode.BadRequest, codes.ClientRequestWrong);
            }
        }

        [HttpPut]
        [Route("rewardStatus/{status}")]
        public IHttpActionResult storeStatus(bool status)
        {
            log.Info(string.Format("In storeStatus. input => status = {0}", status));
            try
            {
                bool Result = _candidateService.storeStatus(base.CandidateId, status);
                log.Info(string.Format("Saved reward status {0}", status));
                return Ok(codes.RequestOk);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                return Content(HttpStatusCode.BadRequest, codes.ClientRequestWrong);
            }
        }

        [HttpPut]
        [Route("{cid}/jigsaw/{value}")]
        public IHttpActionResult jigsawResult(int cid, bool value)
        {
            log.Info(string.Format("In jigsawResult.  input =>candidateId = {0} value={1}", cid, value));
            try
            {  
                bool Result = _candidateService.jigsawResult(cid, value);
                log.Info(string.Format("Saved jigsaw result"));
                return Ok(codes.RequestOk);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Unhandled exception: {0}\r\n{1}", e.Message, e.StackTrace));
                return Content(HttpStatusCode.BadRequest, codes.ClientRequestWrong);
            }
        }
    }
}
