import React from 'react';
import './Task.css';
import TaskList from '../tasklist/TaskList';

class Task extends React.Component {
    state = {
        tasks: []
    }

    render() {
        return (<div className="container">
                    <h3><b>Tasks</b></h3>
                    <TaskList></TaskList>
                </div>);
    }
}

export default Task;