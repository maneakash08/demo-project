import React from 'react';
import './TaskList.css';
import api from '../../api/api';

class TaskList extends React.Component {

    state = {
        tasks: [],
        searchtext: " "
    }

    componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        var data = await api.getData();
        console.log(data);
        this.setState({
            tasks: data
        });

        this.presentData = data;
    }

    completed(status) {        
        return (status === 0) ? <div>true</div>:<div>false</div>; 
    }

    taskList = () => {
        return this.state.tasks.map((task) => (
            <tr key={task.id} id={task.id}>
                <td>{task.id}</td>
                <td>{task.title}</td>
                <td>{(this.completed(task.completed))}</td>
            </tr>
        ));
    }

    searchText = (event) => {
        this.setState({
            searchtext: event.target.value
        });

        let currentList = [];
        let newList = [];

        if (event.target.value !== "") {
            currentList = this.presentData;

            newList = currentList.filter(item => {
                const taskTitle = item.title.toLowerCase();
                const filter = event.target.value.toLowerCase();

                return taskTitle.includes(filter);
            });
        } else {
            newList = this.presentData;
        }

        this.setState({
            tasks: newList
        });

        // if (event.target.value === "") {
        //     this.setState({
        //         tasks: this.presentData
        //     });
        // }
    }

    render() {
        return (<div>
            <input type="text" className="form-control" placeholder="Search" onChange={this.searchText} />
            <h3><i>{this.state.searchtext}</i></h3>
            <table className="table table-bordered mt-3">
                <thead className="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.taskList()}
                </tbody>
            </table>
        </div>);
    }
}

export default TaskList;