import axios from 'axios';

// const url = "https://jsonplaceholder.typicode.com/todos";
const url = "http://localhost:53373/tasks";
// const headers = {
//     'Accept': 'application/json',
//     'Content-Type': 'application/json'
// }

const api = {
    async getData() {
        const { data } = await axios.get(url).catch((error) => {
            console.log("%cProblem Occurrd", 'color:red;font-weight:bold;');
        });
        return data;
    }
}

export default api;