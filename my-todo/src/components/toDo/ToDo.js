import React, { Suspense } from 'react';
import BaseEventEmitter from '../../api/emitter';
import { connect } from 'react-redux';
import middleware from './../../middleware/middleware';
import ContactForm from '../form/ContactForm';
import Block_UI from '../Block-Ui/Block_UI';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

const TaskList = React.lazy(() => import('./../tasklist/TaskList'));

export const emitter = new BaseEventEmitter();

class ToDo extends React.Component {

    state = {
        value: "",
        searchText: "",
        tasks: [],
        blocking: true
    }

    componentDidMount() {
        this.props.getToDoTasks().then(() => {
            setInterval(() => {
                this.setState({ blocking: false });
            }, 3000);
        });

        emitter.onMany(['add_task', 'edit_task', 'delete_task'], (payload) => {
            console.log('Emitter:', payload);
            this.toggleBlock();
        });
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    addTask = () => {
        this.setState({ blocking: true });

        var checkTask = this.props.tasks.filter((task) => (
            task.title === this.state.value
        ));

        if (this.state.value.length === 0) {
            alert("Enter Task Name");
        } else if (checkTask.length !== 0) {
            alert("Task is Already Present");
        } else {
            var task = {
                title: this.state.value
            };
            this.props.createTask(task).then(() => {
                this.props.getToDoTasks();
                emitter.emit('add_task', task);
            });
        }
    }

    toggleBlock = () => {
        this.setState({ blocking: true });
    }

    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <Suspense fallback={<div> Loading... </div>}>
                        <div className="col-4 mt-4">
                            <div className="container row">
                                <input className="col-md-8 form-control" placeholder="Enter Task Name" type="text" name="task-title" value={this.state.value} id="task-id" onChange={this.handleChange} />
                                <input className="col-md-3 ml-2 btn btn-success" type="button" value="Insert" onClick={(e) => this.addTask(e)} />
                            </div>
                            <label>Task List Count:{this.props.tasks.length}</label>
                        </div>
                    </Suspense>

                    <BlockUi tag="div" blocking={this.state.blocking}>
                        <div>
                            <label className="ml-5 pl-4">Redux Task List</label>
                            <ul>
                                <Suspense fallback="...Loading">
                                    <TaskList deleteTask={this.props.deleteTask} editTask={this.props.editTask} getSingleTask={this.props.getSingle} />
                                </Suspense>
                            </ul>
                        </div>
                    </BlockUi>
                </div>

                <div>
                    {/* <ContactForm /> */}
                    <Block_UI></Block_UI>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    console.log("%cToDo State:", "color:blue", state.tasks);
    return {
        tasks: state.tasks
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getToDoTasks: () => middleware.fetchTasksWithRedux(dispatch),
        createTask: task => middleware.postTaskWithRedux(task),
        deleteTask: id => middleware.deleteTaskWithRedux(id, dispatch),
        editTask: task => middleware.editTaskWithRedux(task, dispatch),
        getSingle: id => middleware.getSingleTask(id, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);

