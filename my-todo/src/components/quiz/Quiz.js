import React, { Component } from 'react';

class Quiz extends Component {

    state = {
        formElements: [
            {
                type: "radio",
                required: true,
                name: "Q1",
                value: "ABC",
                key: "q1",
                options: [{
                    required: true,
                    key: "1",
                    name: "r-1",
                    value: "One"
                },
                {
                    required: true,
                    key: "1",
                    name: "r-2",
                    value: "Two"
                }]
            },
            {
                type: "radio",
                required: false,
                name: "Q2",
                value: "XYZ",
                key: "q2",
                options: [{
                    required: false,
                    key: "2",
                    name: "r-3",
                    value: "Three"
                },
                {
                    required: false,
                    key: "2",
                    name: "r-4",
                    value: "Four"
                },]
            },
            {
                type: "text",
                required: true,
                name: "Q3",
                key: "q3",
                placeholder: "PQR-Q3"
            },
            {
                type: "select",
                required: false,
                name: "Q4",
                value: "Select Option",
                key: "q4",
                options: [
                    {
                        required: false,
                        key: "select",
                        name: "select",
                        value: "Select"
                    }, {
                        required: false,
                        key: "5",
                        name: "5",
                        value: "One"
                    },
                    {
                        required: false,
                        key: "6",
                        name: "5",
                        value: "Two"
                    }
                ]
            },
            {
                type: "checkbox",
                required: false,
                name: "Q5",
                value: "Multiple checkBoxes",
                key: "q5",
                options: [{
                    required: false,
                    key: "C-1",
                    name: "7",
                    value: "One-C1"
                },
                {
                    required: false,
                    key: "C-2",
                    name: "8",
                    value: "Two-C1"
                },]
            }
        ],
        correctAnswers: [
            {
                Id: "q1",
                answer: "One"
            }
            , {
                Id: "q2",
                answer: "Three"
            }, {
                Id: "q3",
                answer: "abc"
            }, {
                Id: "q4",
                answer: "One"
            },
            {
                Id: "q5",
                answer: [
                    {
                        Id: "C-1", ans: "One-C1"
                    }, {
                        Id: "C-2", ans: "Two-C1"
                    }]
            }],
        result: 0,
        validation: 0
    }

    i = [];

    handleChange = (index, event) => {
        this.obj = {
            Id: event.target.id,
            Answer: event.target.value
        };

        console.log(event.target);

        this.existingIds = this.i.map(obj => obj.Id);

        if (!this.existingIds.includes(this.obj.Id)) {
            this.i.push(this.obj);
        } else {
            this.i.forEach((elementCheck, index) => {
                if (elementCheck.Id === this.obj.Id) {
                    this.i[index] = this.obj;
                }
            });
        }
    }

    handleCheckEvent = (index, event) => {
        this.answer = [];
        this.tempObj = { Id: event.target.name, answer: event.target.value };
        this.answer.push(this.tempObj);

        this.obj = {
            Id: event.target.id,
            Answer: this.answer
        }

        this.existingIds = this.i.map(obj => obj.Id);

        if (event.target.checked) {
            if (!this.existingIds.includes(this.obj.Id)) {
                this.i.push(this.obj);
            } else {
                this.i.forEach((elementCheck, index) => {
                    if (elementCheck.Id === this.obj.Id) {
                        this.i[index].Answer.push(this.tempObj);
                    }
                });
            }
        } else {
            this.i.forEach((elementCheck, index) => {
                if (elementCheck.Id === this.obj.Id) {
                    elementCheck.Answer.forEach((ans, i) => {
                        if (ans.Id === this.tempObj.Id) {
                            elementCheck.Answer.splice(i, 1);
                        }
                    });

                    if (elementCheck.Answer.length === 0) {
                        this.i.splice(index, 1);
                    }
                }
            });
        }
    }

    getElement = (frm) => {
        var returnElement;

        if (frm.type === "radio" || frm.type === "checkbox") {
            returnElement = frm.options.map((option, ind) => (
                <div key={ind}>
                    <input
                        id={frm.key}
                        key={option.name}
                        type={frm.type}
                        name={option.key}
                        value={option.value}
                        required={option.required}
                        onChange={frm.type === "radio" ? this.handleChange.bind(this, ind) : this.handleCheckEvent.bind(this, ind)} />
                    <label key={ind} htmlFor="">{option.value}</label>
                </div>
            ));
        }

        if (frm.type === "text") {
            returnElement = <div key="txt">
                <input
                    id={frm.key}
                    key={frm.key}
                    type={frm.type}
                    name={frm.key}
                    required={frm.required}
                    placeholder={frm.placeholder}
                    onChange={this.handleChange.bind(this, 0)}
                    required={frm.required}/>
                <label key={frm.name} htmlFor="">{frm.value}</label>
            </div>
        }

        if (frm.type === "select") {
            returnElement = (<div key="slt"><select id={frm.key} onChange={this.handleChange.bind(this, 0)}>
                {frm.options.map((option, ind) => (
                    <option key={ind} value={option.value}>{option.value}</option>
                ))
                }
            </select></div>);
        }

        return returnElement;
    }

    submitQuiz = () => {
        var res = 0;
        var flag = 0;
        this.i.forEach((ans) => {
            if (ans.Answer[0].answer != undefined) {
                ans.Answer.forEach((ans) => {
                    this.state.correctAnswers.forEach((a) => {
                        if (a.answer[0].ans !== undefined) {
                            a.answer.forEach((sd) => {
                                if (ans.answer === sd.ans && ans.Id === sd.Id) {
                                    flag++;
                                    if (a.answer.length === flag) {
                                        res++;
                                    }
                                }
                            });
                        }
                    });
                });
            } else {
                this.state.correctAnswers.forEach((a) => {
                    if (ans.Answer === a.answer && ans.Id === a.Id) {
                        res++;
                    }
                });
            }
        });
        console.log("Submitted:%c", "color:cyan", this.i);
        console.log("Quiz Result:%c", "color:green", res);
    }

    render() {
        return (
            <>
                {this.state.formElements.map((frm, index) => (
                    <React.Fragment key={frm.key}>
                        <hr />
                        <label key={index} htmlFor={frm.key}>{frm.value}</label>
                        {this.getElement(frm, frm.options, index)}
                    </React.Fragment>
                ))}
                <input type="button" value="Submit Quiz" onClick={this.submitQuiz} />
            </>)
    }
}

export default Quiz