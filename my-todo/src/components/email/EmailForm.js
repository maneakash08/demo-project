import React, { Component } from 'react';

class EmailForm extends Component {
    constructor(props) {
        super(props);
        this.val = [];

        this.form = [
            {
                type: "file",
                required: false,
                name: "aadhar",
                label: "Aadhar"
            },
            {
                type: "file",
                required: false,
                name: "pan",
                label: "Pan"
            }
        ];
    }

    state = {
        value: []
    }

    handleChange(index, event) {
        this.val[index] = { key: event.target.name, value: event.target.ref };

        console.log("event:",event.target);  
        console.log("event",event.target.value);
        
        this.setState({
            value: this.val
        });
    }

    submit = () => {
        console.log("Uploaded Files:",this.state.value);
        console.warn(this.fileUpload);
    }

    render() {
        return (<React.Fragment>
            <h3>Email</h3>
            <div>
                <hr />
                <input type="text" name="message" placeholder="Enter Message" id="mail-message" />
                <hr />
                {
                    this.form.map((element, index) => (
                        <input
                            key={index}
                            type={element.type}
                            required={element.required}
                            name={element.name}
                            onChange={this.handleChange.bind(this, index)}
                            ref = { (ref) => this.val[index] = ref } />
                    ))
                }
                <input type="button" value="Send Mail" onClick={this.submit} />
            </div>
        </React.Fragment>);
    }
}

export default EmailForm;