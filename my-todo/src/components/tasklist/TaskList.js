import React, { Component } from 'react';
import { emitter } from './../toDo/ToDo';
import { connect } from 'react-redux';

class TaskList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tasks: ""
        }
    }

    // presentData = () => {
    //     return this.props.tasks;
    // }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState(nextProps);
            console.log("Props Changed");
        }
    }

    editTask = (event) => {
        var taskTitle = prompt("Please enter Task name", event.target.name);
        
        this.props.getSingleTask(event.target.id).then((task) => {
            console.log("SINGLE_TASK:%c","color:green",task);
        })

        var task = {
            id: event.target.id,
            title: taskTitle
        }

        this.props.editTask(task).then(() => {
            emitter.emit('edit_task', task);
        });
    }

    deleteTask = (event) => {
        var payload = {
            id: event.target.id
        }
        this.props.deleteTask(payload).then(() => {
            emitter.emit('delete_task', payload);
        });
    }

    searchTask = (event) => {
        let currentList = [];
        let newList = [];

        if (event.target.value !== "") {
            currentList = this.props.tasks;

            newList = currentList.filter(item => {
                const taskTitle = item.title.toLowerCase();
                const filter = event.target.value.toLowerCase();

                return taskTitle.includes(filter);
            });

            newList = this.props.tasks.filter(item => {
                return item.title.toLowerCase().includes(event.target.value.toLowerCase());
            });

        } else {
            newList = this.props.tasks;
        }

        this.setState({
            tasks: newList
        });

        // console.log(this.state.tasks);

        // if (event.target.value === "") {
        //     console.log(this.presentData());

        //     this.setState({
        //         tasks: this.presentData()
        //     });
        // }
    }


    taskList = () => {
        return (this.state.tasks && this.state.tasks.length > 0) ?
            this.state.tasks.map((task) => (
                <li key={task.id} className="card mt-2">
                    <div className="card-header">
                        {task.title} <div className="badge badge-secondary">{task.id}</div>
                    </div>
                    <div className="row">
                        <input type="button" className="btn col-6" id={task.id} value="Delete" onClick={(e) => this.deleteTask(e)} />
                        <input type="button" className="btn col-6" id={task.id} name={task.title} value="Edit" onClick={(e) => this.editTask(e)} />
                    </div>
                </li>)
            ) : <div className="text-center"> <b> No Task Present </b> </div>
    }

    render() {
        return (<React.Fragment>
            <input type="text" placeholder="Enter Task name" name="search-data" id="search-id" onChange={this.searchTask} />
            <div>{this.taskList()}</div>
        </React.Fragment>);
    }
}

const mapStateToProps = (state) => {
    console.log("%cTaskList State:", "color:gray", state.tasks);
    return {
        tasks: state.tasks
    }
}

export default connect(mapStateToProps)(TaskList);
