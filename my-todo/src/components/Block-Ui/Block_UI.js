import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class Block_UI extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        blocking: false
    }

    toggleBlockUI = () => {
        this.setState({ blocking: !this.state.blocking });
        console.log(this.state);
    }

    render() {
        return (
            <>
                <BlockUi tag="div" blocking={this.state.blocking}>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                     Mollitia at quisquam dolorum illum officia nemo debitis
                     dolor quibusdam veritatis harum repudiandae facilis, dol
                     ore voluptatem saepe iusto in sequi, obcaecati temporibus?
                </BlockUi>

                <input type="button" value="Block-UI" onClick={this.toggleBlockUI} />
            </>
        );
    }
}

export default Block_UI