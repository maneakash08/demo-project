import React from 'react';
import './HeaderNav.css';
import logo from '../../assets/logo.svg';

class HeaderNav extends React.Component {

    state = {
        currentTime: new Date().toLocaleTimeString()
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                currentTime: new Date().toLocaleTimeString()
            });
        }, 1000);
    }

    render() {
        const float = {
            float: 'right'
        }

        return (
            <header className="header">
                <img src={logo} className="App-header-logo pb-2" alt="logo" />
                <b> <label className="react-label p-2">React</label> </b>
                <span style={float} className="text-white p-4"><b> {this.state.currentTime}</b></span>
            </header>
        );
    }
}

export default HeaderNav;