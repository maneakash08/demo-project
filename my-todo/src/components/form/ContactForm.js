import React, { Component } from 'react';

class ContactForm extends Component {

    constructor() {
        super();
        this.val = [];
        this.form = [
            {
                type: "text",
                placeholder: "Enter Email",
                required: false,
                name: "Email",
                value: ""
            },
            {
                type: "text",
                placeholder: "Enter Email",
                required: false,
                name: "Cell",
                value: ""
            },
            {
                type: "text",
                placeholder: "Enter Phone",
                required: false,
                name: "Phone",
                value: ""
            }
        ];
    }

    state = {
        value: []
    }

    handleChange(index, event) {
        this.val[index] = { key: event.target.name, value: event.target.value };

        this.setState({
            value: this.val
        });
    }

    submitData = () => {
        console.log("%cSubmitted", "color:cyan", this.state);
        // this.form.forEach((frm, index) => {
        //     alert(frm.name + ": " + this.state.value[index].value);
        // });
    }

    render() {
        return (
            <div>
                <h2>Contact Us</h2>
                {
                    this.form.map((frm, index) => (
                        <input key={frm.name}
                            type={frm.type}
                            name={frm.name}
                            required={frm.required}
                            placeholder={frm.placeholder}
                            onChange={this.handleChange.bind(this, index)} />
                    ))
                }
                <input type="button" name="btn" value="Submit" onClick={this.submitData} />
            </div>
        );
    }
}

export default ContactForm;





