import React, { Suspense } from 'react';
import './App.css';
import HeaderNav from '../header/HeaderNav';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from '../../Redux/reducers/reducers';
import EmailForm from '../email/EmailForm';
import Quiz from '../quiz/Quiz';

const ToDo = React.lazy(() => import('../toDo/ToDo'));

const store = createStore(rootReducer);

class App extends React.Component {

  componentDidMount() {
    store.subscribe(() => {
      console.log("%cChanges Made in Store:", "color:red", store.getState());
    });
  }

  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <HeaderNav />
          <div className="container mt-4">
            <Suspense fallback={<div className="d-flex align-items-center">
              <strong>Loading...</strong>
              <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>}>
              <ToDo />
            </Suspense>
            {/* <EmailForm /> */}
            <Quiz/>
          </div>
        </React.Fragment>
      </Provider>
    );
  }
}

export default App;
