import { deleteTask, getTodosSuccess, editTask, getSingleTask } from '../Redux/actions/action';
import apiHelper from '../api/api';

const middleware = {
    fetchTasksWithRedux(dispatch) {
        return apiHelper.getData().then((task) => {
            dispatch(getTodosSuccess(task.data));
        });
    },

    postTaskWithRedux(toInsert) {
        return apiHelper.postData(toInsert);
    },

    deleteTaskWithRedux(toBeDeleted, dispatch) {
        return apiHelper.deleteData(toBeDeleted.id).then(() => {
            dispatch(deleteTask(toBeDeleted.id));
        });
    },

    editTaskWithRedux(task, dispatch) {
        return apiHelper.putData(task).then(() => {
            dispatch(editTask(task));
        });
    },

    addManyTaskWithRedux(toInsert) {
        return apiHelper.addMultipletask(toInsert);
    },

    getSingleTask(id, dispatch) {
        return apiHelper.getSingleData(id).then((task) => {
            console.log(task.data);            
            dispatch(getSingleTask(task.data));
        });
    }    
}

export default middleware;