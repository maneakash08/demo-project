import axios from 'axios';
import { Promise, reject } from 'q';

const url = "http://localhost:53373/tasks";
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

const helper = {
    async getData() {
        return await axios.get(url).catch((error) => {
            console.log("%cProblem Occurrd While Retriving", 'color:red;font-weight:bold;');
            return new Promise((res, rej) => {
                return reject();
            });
        });
    },
    async getSingleData(Id) {
        return await axios.get(url + '/' + Id).catch((error) => {
            console.log("%cProblem Occurrd While Retriving", 'color:red;font-weight:bold;');
            return new Promise((res, rej) => {
                return reject();
            });
        })
    },
    async postData(toInsert) {
        return await axios.post(url, toInsert, headers).catch((error) => {
            console.log("%cProblem Occurrd While Inserting", 'color:red;font-weight:bold;');
            return new Promise((res, rej) => {
                return reject();
            });
        });
    },
    async putData(toUpdate) {
        return await axios.put(url + '/' + toUpdate.id, { title: toUpdate.title }).catch((error) => {
            console.log("%cProblem Occurrd While Updating", 'color:red;font-weight:bold;');
            return new Promise((res, rej) => {
                return reject();
            });
        });
    },
    async deleteData(id) {
        return await axios.delete(url + '/' + id).catch((error) => {
            console.log("%cProblem Occurrd While Deleting", 'color:red;font-weight:bold;');
            return new Promise((res, rej) => {
                return reject();
            });
        });
    },
    async addMultipletask(taskList) {
        taskList.forEach(task => {
            axios.post(url, task, headers).catch((error) => {
                console.log();
                return new Promise((res, rej) => {
                    return reject();
                });
            });
        });
        await taskList;
    }
}

export default helper;