import { EventEmitter } from "events";

export default class BaseEventEmitter {
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  on(eventName, listener) {
    this.eventEmitter.on(eventName, listener);
  }

  off(eventName, listener) {
    this.eventEmitter.removeListener(eventName, listener);
  }

  emit(event, payload, error = false) {
    this.eventEmitter.emit(event, payload, error);
  }

  onMany(events, listener) {
    events.forEach(event => {
      this.eventEmitter.on(event, listener);
    });
  }

};
