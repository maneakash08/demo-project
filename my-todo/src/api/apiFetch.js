// Without using Axios Only Javascript Fetch

const helper = {
    dbHelper(method, body) {

        switch (method) {
            case 'GET':
                fetch('http://localhost:53373/tasks').then((res) => (
                    res.json()
                )).then((tasks) => {
                    return tasks;
                }).catch((error) => {
                    console.log(error);
                    alert("something went wrong");
                });
                break;

            case 'POST':
                fetch('http://localhost:53373/tasks', {
                    method: 'post',
                    body: JSON.stringify(body),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                }).then((response) => {
                    console.log(response);
                }).then(() => {
                    this.dbHelper('GET');
                }).catch((error) => {
                    alert("something went wrong");
                });
                break;

            case 'PUT':
                fetch('http://localhost:53373/tasks/' + body.id, {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ title: body.title })
                }).then((response) => {
                    console.log(response);
                }).then(() => {
                    this.dbHelper('GET');
                }).catch((error) => {
                    alert("something went wrong");
                });
                break;

            case 'DELETE':
                fetch('http://localhost:53373/tasks/' + body, {
                    method: method,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ id: body })
                }).then((response) => {
                    console.log(response);
                }).then(() => {
                    this.dbHelper('GET');
                }).catch((error) => {
                    alert("something went wrong");
                });

                break;
        }
    }
}

export default helper