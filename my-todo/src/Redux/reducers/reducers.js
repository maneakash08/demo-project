import { ADD_TASK, GET_SUCCESS, DEL_TASK, EDIT_TASK, ADD_TASK_MANY, GET_SINGLE } from '../actions/action';

const initialState = {
  tasks: [],
  task: {}
};

function rootReducer(state = initialState, action) {

  if (action.type === ADD_TASK) {
    return { ...state, tasks: action.payload };
  }

  if (action.type === GET_SUCCESS) {
    return { ...state, tasks: action.payload };
  }

  if (action.type === DEL_TASK) {
    return { ...state, tasks: state.tasks.filter((task) => task.id !== Number(action.payload)) };
  }

  if (action.type === ADD_TASK_MANY) {
    return { ...state, tasks: action.payload }
  }

  if (action.type === EDIT_TASK) {
    var data = state.tasks;

    data.forEach((task) => {
      if (task.id === Number(action.payload.id)) {
        task.title = action.payload.title;
      }
    });
    return { ...state, tasks: data };
  }

  if (action.type === GET_SINGLE) {
    return { ...state, task: action.payload }
  }

  return state;
}

export default rootReducer;