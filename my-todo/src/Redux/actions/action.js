
export const ADD_TASK = 'ADD_TASK'
export const DEL_TASK = 'DEL_TASK'
export const GET_SUCCESS = 'GET_SUCCESS'
export const EDIT_TASK = 'EDIT_TASK'
export const ADD_TASK_MANY = 'ADD_TASK_MANY'
export const GET_SINGLE = 'GET_SINGLE'

export function addTodo(payload) {
  return { type: ADD_TASK, payload }
}

export function getTodosSuccess(payload) {
  return { type: GET_SUCCESS, payload }
}

export function deleteTask(payload) {
  return { type: DEL_TASK, payload }
}

export function editTask(payload) {
  return { type: EDIT_TASK, payload }
}

export function addManyTask(payload) {
  return { type: ADD_TASK_MANY, payload }
}

export function getSingleTask(payload) {
  console.log(payload);
  
  return { type: GET_SINGLE, payload }
}

