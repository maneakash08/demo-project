﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CacheManager.Core;
using Coyote.Common.Cache;
using Coyote.Common.Cache.Managers;
using Coyote.Common.Cache.Managers.Configuration;

namespace RedisExample
{
    public class CacheConfigService<T>
    {
        private readonly CacheManager<T> _cacheManager;
        private readonly RedisConfiguration _redisConfiguration;
        public CacheConfigService(RedisConfiguration redisConfiguration)
        {
            _redisConfiguration = redisConfiguration;
            _cacheManager = new CacheManager<T>(_redisConfiguration);
        }

        public async Task Add(string key, T value)
        {
            await _cacheManager.AddAsync(key, value);
        }

        public async Task ClearAsync()
        {
            await _cacheManager.ClearAsync();
        }

        public async Task<T> GetFromCacheAsync(string key)
        {
            return await _cacheManager.GetFromCacheAsync(key);
        }
    }
}
