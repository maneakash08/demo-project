﻿using CacheManager.Core;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;
using Coyote.Common.Cache;
using Coyote.Common.Cache.Managers.Configuration;
using System.Timers;
using System.Net;
using System.Configuration;
using System.Collections.Generic;
using System.Threading;

namespace RedisExample
{
    public class Model
    {
        public Model(string Key, string Value)
        {
            this.Key = Key;
            this.Value = Value;
        }
        public string Key { get; set; }
        public string Value { get; set; }
    }

    class Program
    {
        static string AzureHost = "redis-service.redis.cache.windows.net";
        static int AzurePort = 6380;

        static string LocalHost = "localhost";
        static int LocalPort = 6379;

        // Redis Connection
        private readonly CacheConfigService<string> _cacheManager;
        private readonly ConnectionMultiplexer connRedisLocal;
        private readonly ConnectionMultiplexer connRedisAzure;

        // Redis Database
        private readonly IDatabase redisDbLocal;
        private readonly IDatabase redisDbAzure;

        // Validation 
        private static bool _distributedEnabled = false;
        private static bool _isLocalCacheUpdated = false;

        // Pub Sub
        readonly string channelName = "test-pub-sub";
        private readonly ISubscriber subscriber;
        private readonly ISubscriber publisher;

        // List of Data
        List<Model> cacheDataList;

        // Lock Object
        private static readonly Object _lockObj = new object();
        private ReaderWriterLockSlim lockSlim = new ReaderWriterLockSlim();

        public Program()
        {
            var cacheConfigConnection = GetRedisConfiguration();
            _cacheManager = new CacheConfigService<string>(cacheConfigConnection);

            try
            {
                connRedisAzure = ConnectionMultiplexer.Connect(ConfigurationManager.AppSettings["CacheConnection"]);
            }
            catch (Exception ex)
            {
                _distributedEnabled = true;
                throw ex;
            }

            connRedisLocal = RedisConnectorHelper.Connection;
            cacheDataList = new List<Model>();

            redisDbLocal = GetRedisConnection(connRedisLocal);
            redisDbAzure = connRedisAzure.GetDatabase();

            //Publisher Subscriber Initialize 
            subscriber = connRedisAzure.GetSubscriber();
            publisher = connRedisAzure.GetSubscriber();
        }

        static void Main(string[] args)
        {
            var program = new Program();
            try
            {
                //Task.Run(async () => await program.SaveDataToCache("E", "0XYZ"));
                program.ShowCacheData();
                program.PublishSubscribe();
            }
            catch (RedisException rExp)
            {
                _distributedEnabled = true;
                Console.WriteLine("Redis Exception:" + rExp);
            }
            Console.ReadKey();
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e, bool isCacheSavable)
        {
            //timer = new System.Threading.Timer(1000)
            //{ 
            //    timer.Elapsed += (sender, e) => Timer_Elapsed(sender, e, false);
            //    Interval = 3000,
            //    Enabled = false
            //};

            Console.WriteLine("Timer");
        }

        public IDatabase GetRedisConnection(ConnectionMultiplexer connRedis)
        {
            connRedis.ConnectionFailed += (sender, args) =>
            {
                _distributedEnabled = true;
                Console.WriteLine("\nConnection failed, disabling redis...:" + _distributedEnabled);
                Task.Run(async () => await redisDbAzure.StringSetAsync("new", "test"));
                _isLocalCacheUpdated = true;
                //Task.Run(async () => await SaveDataToCache("new", "test"));
            };

            connRedis.ConnectionRestored += (sender, args) =>
            {
                Console.WriteLine("Connection Restored");
                publisher.Publish(channelName, "Connection Restored:Data Updated");
                Task.Run(async () => await SetLatestDataToCache(connRedisAzure, connRedisLocal, AzureHost, AzurePort));
                //ShowCacheData();
            };

            return connRedis.GetDatabase();
        }

        public void PublishSubscribe()
        {
            subscriber.Subscribe(channelName, (ch, msg) =>
            {
                Console.WriteLine("\nSubscriber:" + msg);
                Task.Run(async () => await SetLatestDataToCache(connRedisLocal, connRedisAzure,LocalHost,LocalPort));
            });

            publisher.Subscribe(channelName, (channel, message) =>
            {
                var date = DateTime.Now.ToLongTimeString();
                Console.WriteLine("\nPublisher:" + date + ":: Message:" + message);
            });
        }

        public async Task SetLatestDataToCache(ConnectionMultiplexer sender, ConnectionMultiplexer reciever,string host, int port)
        {
            List<Model> models = new List<Model>();
            IDatabase cacheDatabaseSender = sender.GetDatabase();
            IDatabase cacheDatabaseReciever = reciever.GetDatabase();

            if (_isLocalCacheUpdated && !_distributedEnabled)
            {
                var _server = connRedisAzure.GetServer("redis-service.redis.cache.windows.net", 6380);
                var _keys = _server.Keys();

                foreach (var key in _keys)
                {
                    var val = connRedisAzure.GetDatabase().StringGet(key);
                    models.Add(new Model(key, val));
                }
                _isLocalCacheUpdated = false;
            }

            if (models.Count != 0)
            {
                var server = connRedisLocal.GetServer("localhost", 6379);
                var keys = server.Keys();

                foreach (var key in keys)
                {
                    var val = redisDbLocal.StringGet(key);
                    await redisDbAzure.StringSetAsync(key, val);
                }

                foreach (var model in models)
                {
                    await redisDbLocal.StringSetAsync(model.Key, model.Value);
                }
            }
            else
            {
                lockSlim.EnterWriteLock();
                try
                {
                    var server = sender.GetServer(host, port);
                    var keys = server.Keys();

                    foreach (var key in keys)
                    {
                        var val = await cacheDatabaseSender.StringGetAsync(key);
                        await cacheDatabaseReciever.StringSetAsync(key, val);
                    }
                }
                finally
                {
                    lockSlim.ExitWriteLock();
                }
            }
        }

        public async Task SaveDataToCache(string key, string value)
        {
            lockSlim.EnterWriteLock();
            try
            {
                //LocalCache
                var localResult =  await redisDbLocal.StringSetAsync(key, value);
                
                //AzureCache
                if (!_distributedEnabled)
                {
                    var res = await redisDbAzure.StringSetAsync(key, value);
                    if (res)
                    {
                        _isLocalCacheUpdated = true;
                    }
                }
            }
            finally
            {
                publisher.Publish(channelName, "New Data is Added");
                lockSlim.ExitWriteLock();
            }
        }

        public List<Model> GetAllValuesFromCache(ConnectionMultiplexer connection, string serverName, int port)
        {
            cacheDataList.Clear();

            var cacheDatabase = connection.GetDatabase();
            var server = connection.GetServer(serverName, port);
            var keys = server.Keys();

            foreach (var key in keys)
            {
                var val = connection.GetDatabase().StringGet(key);
                cacheDataList.Add(new Model(Key: key, Value: val));
            }

            return cacheDataList;
        }

        public async Task<string> GetSingleDataFromCache(string key,string type)
        {
            return type != "azure" ? await redisDbLocal.StringGetAsync(key) : await redisDbAzure.StringGetAsync(key);
        }
        
        public void ShowCacheData()
        {
            Console.WriteLine("Azure Cache Data:\n");
            foreach (var data in GetAllValuesFromCache(connRedisAzure, AzureHost, AzurePort))
            {
                Console.WriteLine("Key:" + data.Key + " " + "Value:" + data.Value);
            }

            Console.WriteLine("\nLocal Cache Data:\n");
            foreach (var data in GetAllValuesFromCache(connRedisLocal, LocalHost, LocalPort))
            {
                Console.WriteLine("Key:" + data.Key + " " + "Value:" + data.Value);
            }
        }
        public RedisConfiguration GetRedisConfiguration()
        {
            var cacheConfiguration = new RedisConfiguration
            {
                RedisEnabled = true,
                RedisConnection = ConfigurationManager.AppSettings["CacheConnection"],
                RedisDatabaseId = 0,
                RedisBackplaneEnabled = true,
                CacheName = "RedisCache",
                CacheHandle = "RedisCacheHandle",
                EnableKeySpaceNotifications = false
            };

            return cacheConfiguration;
        }
    }
}

