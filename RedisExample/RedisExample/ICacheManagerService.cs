﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisExample
{
    public interface ICacheManagerService<T>
    {
        Task Add(string key, T value);
        Task ClearAsync();
        Task<T> GetFromCacheAsync(string key);
    }
}
