﻿using System;
using StackExchange.Redis;
using Coyote.Common.Cache.Managers.Configuration;
using System.Configuration;

namespace RedisExample
{
    public class RedisConnectorHelper
    {
        static RedisConnectorHelper()
        {
            RedisConnectorHelper.lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                //For Local Cache (In-Memory Cache)
                var redisSecret = "localhost:6379";
                //var redisSecret = ConfigurationManager.AppSettings["CacheConnection"];
                var connRedis = ConnectionMultiplexer.Connect(redisSecret);
                return connRedis;
            });
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection;

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
