﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TestWebApi.Models;

namespace TestWebApi.Helpers
{
    public class MailHelper
    { 
        public static async Task SendMail(Message message)
        {
            try
            {
                String SendGridApiKey = System.Configuration.ConfigurationManager.AppSettings["sendGridApiKey"];
                String Mailid = System.Configuration.ConfigurationManager.AppSettings["mailId"];

                var client = new SendGridClient(SendGridApiKey);

                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(Mailid, "Test Email Address"),
                    Subject = message.Subject
                };

                //HttpFileCollection files = HttpContext.Current.Request.Files;

                //foreach (String key in files.AllKeys)
                //{
                //    int did = Int32.Parse(key);
                //    HttpPostedFile hpf = files[i];
                //    BinaryReader b = new BinaryReader(hpf.InputStream);
                //    string contents = String.Empty;
                //    using (var memoryStream = new MemoryStream())
                //    {
                //        hpf.InputStream.CopyToAsync(memoryStream).Wait();
                //        byte[] stream = memoryStream.ToArray();
                //        string content = Convert.ToBase64String(stream);
                //        contents = content;
                //    }

                //    msg.HtmlContent = message;
                //    msg.AddAttachment(hpf.FileName, contents);
                //    i++;
                //}

                msg.AddTo(new EmailAddress(message.MailTo, message.Sender));
                var response = await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}