﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWebApi.Models
{
    public class Message
    {
        public string Sender { get; set; }
        public String Message_Content { get; set; }
        public String Subject { get; set; }
        public String MailTo { get; set; }
    }
}