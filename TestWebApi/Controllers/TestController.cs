﻿using System;
using System.Collections.Generic;
using System.Linq;
using SendGrid.Helpers.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using TestWebApi.Models;

namespace TestWebApi.Controllers
{
    [RoutePrefix("tasks")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TestController : ApiController
    {

        [HttpGet]
        [Route("")]
        public List<todo> Get()
        {
            using(Backbone TestEntities db = new BackboneTestEntities())
            {
                return db.todoes.ToList<todo>();
            }
        }

        [HttpGet]
        [Route("{id}")]
        public todo Get(int id)
        {
            using (BackboneTestEntities db = new BackboneTestEntities())
            {
                return db.todoes.Where(task => task.id == id).SingleOrDefault();
            }
        }

        [HttpPost]
        [Route("")]
        public void Post([FromBody] todo task)
        {
            using (BackboneTestEntities db = new BackboneTestEntities())
            {
                db.todoes.Add(task);
                db.SaveChanges();
            }
        }

        [HttpPut]
        [Route("{id}")]
        public void Put(int id,[FromBody]  todo task)
        {
            using(BackboneTestEntities db = new BackboneTestEntities())
            {
                var to = db.todoes.Where(t => t.id == id).SingleOrDefault();
                to.title = task.title;
                db.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            using (BackboneTestEntities db = new BackboneTestEntities())
            {
                var to = db.todoes.Where(task => task.id == id).SingleOrDefault();
                db.todoes.Remove(to);
                db.SaveChanges();
            }
        }

        [HttpPost]
        [Route("mail")]
        public void SendEmail([FromBody]Message message)
        {
            using (BackboneTestEntities db = new BackboneTestEntities())
            {
                Helpers.MailHelper.SendMail(message).ConfigureAwait(false);
            }
        }
    }
}