import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MyServiceEvent {
  message: string;
  eventId: number;
}

export class EmitterSericeService {

  constructor() { }

  public onChange: EventEmitter<any> = new EventEmitter<any>();

  public doSomething(message: any) {
    this.onChange.emit(message);
  }
}
