import { EmitterSericeService } from './../emitter-serice.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { EmitterSericeService } from '../emitter-serice.service';

@Component({
  selector: 'app-sub-child-component',
  templateUrl: './sub-child-component.component.html',
  styleUrls: ['./sub-child-component.component.scss']
})
export class SubChildComponentComponent implements OnInit {

  @Input() nameData: string;
  @Input() languageData: any;
  @Output() updateLanguages = new EventEmitter();
  monthsData: any;

  constructor(private emitterService: EmitterSericeService) {

    this.monthsData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: 'My Second dataset',
          backgroundColor: '#9CCC65',
          borderColor: '#7CB342',
          data: [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label: 'My Third dataset',
          backgroundColor: 'blue',
          borderColor: 'darkblue',
          data: [18, 78, 15, 29, 76, 77, 80]
        }
      ]
    }
  }

  changeLanguageDataFun() {
    this.updateLanguages.emit("King Arthur");
  }

  ngOnInit() {
    console.warn("Sub-Child", this.languageData);
  }

  emit() {
    this.emitterService.doSomething(this.monthsData);
  }

}
