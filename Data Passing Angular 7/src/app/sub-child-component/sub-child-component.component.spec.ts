import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubChildComponentComponent } from './sub-child-component.component';

describe('SubChildComponentComponent', () => {
  let component: SubChildComponentComponent;
  let fixture: ComponentFixture<SubChildComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubChildComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubChildComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
