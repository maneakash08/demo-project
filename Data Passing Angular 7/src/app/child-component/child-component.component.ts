import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.scss']
})
export class ChildComponentComponent implements OnInit {

  @Input() name: string;
  @Input() languageData: any;
  @Output() updateLanguages = new EventEmitter();

  auth_data: string;

  changeLanguageDataFun(data) {
    this.updateLanguages.emit(data);
    console.log(this.languageData);
  }

  testop() {
    alert("Worked")
  }

  ngOnInit() {
  }
}
