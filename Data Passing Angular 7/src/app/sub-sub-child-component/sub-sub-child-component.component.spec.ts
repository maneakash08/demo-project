import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubChildComponentComponent } from './sub-sub-child-component.component';

describe('SubSubChildComponentComponent', () => {
  let component: SubSubChildComponentComponent;
  let fixture: ComponentFixture<SubSubChildComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubChildComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubChildComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
