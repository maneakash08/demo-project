import { Component, OnInit } from '@angular/core';
import { EmitterSericeService } from '../emitter-serice.service';

@Component({
  selector: 'app-sub-sub-child-component',
  templateUrl: './sub-sub-child-component.component.html',
  styleUrls: ['./sub-sub-child-component.component.scss']
})
export class SubSubChildComponentComponent implements OnInit {

  data: any;

  constructor(private emitterService:EmitterSericeService) {
  }

  ngOnInit() {
    this.emitterService.onChange.subscribe((barChartData)=> {
      this.data = barChartData;
    })
  }

}
