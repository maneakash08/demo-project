import { EmitterSericeService } from './emitter-serice.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildComponentComponent } from './child-component/child-component.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { from } from 'rxjs';
import { SubChildComponentComponent } from './sub-child-component/sub-child-component.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ColorPickerModule } from 'primeng/colorpicker';
import { SliderModule } from 'primeng/slider';
import { SubSubChildComponentComponent } from './sub-sub-child-component/sub-sub-child-component.component';
import { PanelModule } from 'primeng/panel';
import { FieldsetModule } from 'primeng/fieldset';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { FocusTrapModule } from 'primeng/focustrap';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponentComponent,
    SubChildComponentComponent,
    SubSubChildComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccordionModule,
    ButtonModule,
    ChartModule,
    FormsModule,
    InputTextModule,
    ReactiveFormsModule,
    ColorPickerModule,
    SliderModule,
    PanelModule,
    FieldsetModule,
    TabViewModule,
    ToastModule,
    FocusTrapModule
  ],
  providers: [EmitterSericeService],
  bootstrap: [AppComponent],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
