import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmitterSericeService } from './emitter-serice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {

  name = "Chin Yang";
  languageData: any;
  languageDataNew: any;
  _emitterService: any;
  monthsData: any;

  languageDataFormGroup = new FormGroup({
    label: new FormControl(''),
    data: new FormControl(''),
    colorName: new FormControl(''),
    hoverColor: new FormControl('')
  });

  constructor(private emitterService: EmitterSericeService) {
    this.languageData = {
      labels: ['Angular', 'Java', 'C#'],
      datasets: [
        {
          data: [10, 50, 40],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
    };

    this.languageDataNew = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [],
          hoverBackgroundColor: []
        }
      ]
    }

    this._emitterService = this.emitterService.onChange.subscribe(
      (langData) => {
        console.log("%cFrom Emitter:", "color:red", langData);
      }
    );

    this.monthsData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: 'My Second dataset',
          backgroundColor: '#9CCC65',
          borderColor: '#7CB342',
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    }
  }

  onBooksUpdated(nm) {
    this.name = nm;
    console.warn(nm);
  }

  change() {
    this.name = "Richard Hendrix";
  }

  emit() {
    this.emitterService.doSomething(this.monthsData);
  }

  addLanguage() {
    this.languageDataNew = this.languageData;
    this.languageDataNew.labels.push(this.languageDataFormGroup.get('label').value);
    this.languageDataNew.datasets[0].data.push(this.languageDataFormGroup.get('data').value);
    this.languageDataNew.datasets[0].backgroundColor.push(this.languageDataFormGroup.get('colorName').value);
    this.languageDataNew.datasets[0].hoverBackgroundColor.push(this.languageDataFormGroup.get('hoverColor').value);
    console.log(this.languageDataNew);

    this.languageData = Object.assign({}, this.languageDataNew);
    console.log(this.languageData);
  }

  ngOnDestroy() {
  }
}
